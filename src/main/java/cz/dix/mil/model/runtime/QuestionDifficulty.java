package cz.dix.mil.model.runtime;

/**
 * Difficulty of the question.
 * EASY are typically questions 1-5, MID 6-10 and HARD 11+
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public enum QuestionDifficulty {

    EASY, MID, HARD
}
