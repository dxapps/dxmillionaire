package cz.dix.mil.model.runtime;

/**
 * Type of the hint.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public enum Hint {

    AUDIENCE, FIFTY_FIFTY, PHONE_FRIEND
}
