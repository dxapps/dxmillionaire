package cz.dix.mil.model.runtime;

import cz.dix.mil.model.algorithm.AutomaticAudience;
import cz.dix.mil.model.algorithm.AutomaticPhoneFriend;
import cz.dix.mil.model.algorithm.DefaultAutomaticAudience;
import cz.dix.mil.model.algorithm.DefaultAutomaticPhoneFriend;
import cz.dix.mil.model.game.Answer;
import cz.dix.mil.model.game.Game;
import cz.dix.mil.model.game.Question;

import java.util.*;

/**
 * Represents whole model of the game.
 * It means it contains all static data (game questions and answers) but also runtime data like selected answer
 * of player or actual state of game.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class GameModel {

    private final Game game;
    private final AutomaticAudience automaticAudience = new DefaultAutomaticAudience();
    private final AutomaticPhoneFriend automaticPhoneFriend = new DefaultAutomaticPhoneFriend();
    private int actualQuestionIdx = -1;
    private Collection<Hint> availableHints = new ArrayList<>(Arrays.asList(Hint.values()));

    private Answer selectedAnswer = null;
    private Set<Answer> removedAnswers = new HashSet<>();
    private PlayersProgress playersProgress = PlayersProgress.BEFORE_GAME;
    private AudienceVotingResult audienceVotingResult;

    public GameModel(Game game) {
        this.game = game;
    }

    /**
     * Gets a name of the game.
     *
     * @return name of the game
     */
    public String getGameName() {
        return game.getName();
    }

    /**
     * Updates model when player answers with given answer.
     *
     * @param answer answer selected by player
     */
    public void answerQuestion(Answer answer) {
        selectedAnswer = answer;
        if (selectedAnswer.isCorrect()) {
            if (!hasNextQuestion()) {
                playersProgress = PlayersProgress.WON_GAME;
            }
        } else {
            playersProgress = PlayersProgress.AFTER_INCORRECT_ANSWER;
        }
    }

    /**
     * Gets actual question of the game.
     *
     * @return actual question
     */
    public Question getActualQuestion() {
        return game.getQuestion(actualQuestionIdx);
    }

    /**
     * Gets a flag whether there is still any question to be responded.
     *
     * @return true if any question is still available, otherwise false
     */
    public boolean hasNextQuestion() {
        return (actualQuestionIdx + 1) < game.getQuestionsCount();
    }

    /**
     * Moves game to the next question while it clears previously stored data about answer.
     */
    public void toNextQuestion() {
        clearQuestionData();
        if (!hasNextQuestion()) {
            throw new IllegalArgumentException("No more questions in the game");
        }
        actualQuestionIdx++;
    }

    /**
     * Gets a difficulty of actual question.
     *
     * @return actual question difficulty
     */
    public QuestionDifficulty getActualQuestionDifficulty() {
        int questionIdx = getQuestionIdx(getActualQuestion());
        if (questionIdx < 5) {
            return QuestionDifficulty.EASY;
        } else if (questionIdx < 10) {
            return QuestionDifficulty.MID;
        } else {
            return QuestionDifficulty.HARD;
        }
    }

    /**
     * Gets a flag whether given question is checkpoint.
     *
     * @param question question to be checked
     * @return true if question is checkpoint, otherwise false
     */
    public boolean isCheckpoint(Question question) {
        return (getQuestionIdx(question) + 1) % 5 == 0;
    }

    /**
     * Gets a flag whether given hint is available (not used yet) for player.
     *
     * @param hint hint to be checked
     * @return true if hint is still available, otherwise false
     */
    public boolean isHintAvailable(Hint hint) {
        return availableHints.contains(hint);
    }

    /**
     * Uses 50-50 hint and updates available answers, see {@link #isAnswerAvailable(Answer)}}.
     */
    public void useFiftyFifty() {
        availableHints.remove(Hint.FIFTY_FIFTY);

        List<Answer> allAnswers = getActualQuestion().getAnswers();

        while (removedAnswers.size() < (allAnswers.size() / 2)) {
            int randomIdx = (int) (Math.random() * ((double) allAnswers.size()));
            Answer randomAnswer = getActualQuestion().getAnswers().get(randomIdx);
            if (!randomAnswer.isCorrect()) {
                if (!removedAnswers.contains(randomAnswer)) {
                    removedAnswers.add(randomAnswer);
                }
            }
        }
    }

    /**
     * Removes phone friend hint from available hints.
     */
    public void removePhoneFriendHint() {
        availableHints.remove(Hint.PHONE_FRIEND);
    }

    /**
     * Uses phone friend hint and generates result according to automatic algorithm
     *
     * @return result of phone friend
     */
    public PhoneFriendResult generatePhoneFriendResult() {
        removePhoneFriendHint();
        availableHints.remove(Hint.PHONE_FRIEND);
        return automaticPhoneFriend.call(getPossibleAnswers(), getActualQuestionDifficulty());
    }

    /**
     * Uses audience hint and updates values according to given values.
     * Use {@link #getAudienceVotingResult()} to retrieve it.
     *
     * @param counts counts of votes for each answer
     */
    public void setAudienceResults(int[] counts) {
        availableHints.remove(Hint.AUDIENCE);
        audienceVotingResult = new AudienceVotingResult(counts);
    }

    /**
     * Uses audience hint and generates values according to automatic algorithm.
     * Use can use {@link #getAudienceVotingResult()} to retrieve it later.
     *
     * @return result of audience voting
     */
    public AudienceVotingResult generateAudienceResults() {
        availableHints.remove(Hint.AUDIENCE);
        audienceVotingResult = automaticAudience.vote(getActualQuestion().getAnswers(),
                getPossibleAnswers(), getActualQuestionDifficulty());
        return audienceVotingResult;
    }

    /**
     * Gets player's final reward according if he/she gave up, answered incorrectly or won game.
     *
     * @return final reward of player
     */
    public String getFinalReward() {
        switch (playersProgress) {
            case GAVE_UP:
            case WON_GAME:
                return getActualQuestion().getReward();
            case AFTER_INCORRECT_ANSWER:
                Question checkpointQuestion = null;
                int idx = actualQuestionIdx - 1;
                while (idx >= 0 && checkpointQuestion == null) {
                    Question q = game.getQuestion(idx);
                    if (isCheckpoint(q)) {
                        checkpointQuestion = q;
                    }
                    idx--;
                }

                if (checkpointQuestion != null) {
                    return checkpointQuestion.getReward();
                } else {
                    return "Nothing";
                }
            default:
                System.out.println("Game is in incorrect state, please contact authors.");
                System.exit(27);
                return null; // never happens
        }
    }

    /**
     * Gets a flag whether audience results are available (hint was used and data entered).
     *
     * @return true if audience result is available, otherwise false
     */
    public boolean hasAudienceResult() {
        return audienceVotingResult != null;
    }

    /**
     * Checks whether given answer is available (was not removed by 50-50 hint).
     *
     * @param answer answer to be checked
     * @return true if answer is available, otherwise false
     */
    public boolean isAnswerAvailable(Answer answer) {
        return !removedAnswers.contains(answer);
    }

    /**
     * Gets answers that are possible (not removed by 50-50 for example).
     *
     * @return possible answers.
     */
    public List<Answer> getPossibleAnswers() {
        List<Answer> out = new ArrayList<>();
        for (Answer answer : getActualQuestion().getAnswers()) {
            if (isAnswerAvailable(answer)) {
                out.add(answer);
            }
        }
        return out;
    }

    /**
     * Gets all questions of the game.
     *
     * @return all questions
     */
    public List<Question> getAllQuestions() {
        return game.getQuestions();
    }

    /**
     * Gets a progress of the player.
     *
     * @return progress of player
     */
    public PlayersProgress getPlayerProgress() {
        return playersProgress;
    }

    /**
     * Gets selected answer by player (if any).
     *
     * @return selected answer by player or null if no answer selected yet
     */
    public Answer getSelectedAnswer() {
        return selectedAnswer;
    }

    /**
     * Gets index of actual question.
     * Note that the first index is 0.
     *
     * @return index of actual question
     */
    public int getActualQuestionIdx() {
        return actualQuestionIdx;
    }

    /**
     * Gets result of audience hint.
     *
     * @return results of audience hint
     */
    public AudienceVotingResult getAudienceVotingResult() {
        return audienceVotingResult;
    }

    private int getQuestionIdx(Question question) {
        int idx = 0;
        for (Question q : game.getQuestions()) {
            if (q.equals(question)) {
                return idx;
            }
            idx++;
        }
        throw new IllegalArgumentException("Question is not covered by this model");
    }

    private void clearQuestionData() {
        selectedAnswer = null;
        removedAnswers.clear();
        playersProgress = PlayersProgress.IN_GAME;
        audienceVotingResult = null;
    }
}
