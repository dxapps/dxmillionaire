package cz.dix.mil.view.skin;

import java.awt.*;

/**
 * Represents simple gradient of two colors.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class Gradient {

    public Color color1;
    public Color color2;

    public Gradient(Color color1, Color color2) {
        this.color1 = color1;
        this.color2 = color2;
    }
}
